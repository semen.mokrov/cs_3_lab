from PyQt5.QtWidgets import QMessageBox
import inspect


def run_machine(machine, code_file_input, code_file_output):
    module_stack = inspect.stack()
    module_stack.pop()
    module = module_stack.pop()
    module_name = module.frame.f_code.co_name
    try:
        output, control_unit, log_stream = machine.main([code_file_input, code_file_output])
        return output, control_unit, log_stream
    except FileNotFoundError:
        if module_name != '_run_code':
            msg = QMessageBox()
            msg.setWindowTitle('File not found')
            msg.setText('Your file is not found')
            msg.setIcon(QMessageBox.Icon.Critical)

            msg.exec_()
        return None, None, None

    except Exception:
        if module_name != '_run_code':
            msg = QMessageBox()
            msg.setWindowTitle('Machine error')
            msg.setText('Your code is not valid')
            msg.setIcon(QMessageBox.Icon.Critical)

            msg.exec_()
        return None, None, None
