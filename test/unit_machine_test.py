"""
Unit-тесты для процессора
"""
import unittest

from src.machine.config import AMOUNT_OF_MEMORY
from src.machine.machine import ControlUnit
from src.translation.isa import Operation, read_code, Opcode, Register


class MachineTest(unittest.TestCase):
    """Unit-тесты для процессора"""

    input = "resources/input.txt"
    cu = ControlUnit()

    def set_program_and_run(self, code: list[Operation]):
        """Запуск тестовой программы"""
        self.cu.set_program(code, [])
        try:
            while True:
                self.cu.decode_and_execute_instruction()
        except IndexError:
            pass
        except StopIteration:
            pass

    def get_start_addr(self) -> int:
        """Получение стартового адреса"""
        prog_addr: int = 0
        operation: Operation = self.cu.data_path.memory[prog_addr]

        while operation.opcode == Opcode.DATA:
            prog_addr += 1
            operation = self.cu.data_path.memory[prog_addr]

        return prog_addr

    def test_alu_addr(self):
        """Тестирования АЛУ для вычисления адреса"""
        code: list[Operation] = read_code("resources/examples_correct/alu_addr.json")
        self.set_program_and_run(code)

        prog_addr: int = self.get_start_addr()

        self.assertEqual(self.cu.data_path.pc_counter, prog_addr + 1)

    def test_alu_data(self):
        """Тестирование АЛУ для вычисления данных"""
        code: list[Operation] = read_code("resources/examples_correct/alu_data.json")

        self.set_program_and_run(code)
        self.assertEqual(self.cu.data_path.reg_file.registers[Register.R1], -9223372036854775799)
        self.assertEqual(self.cu.data_path.reg_file.registers[Register.R2], 5)

        self.assertEqual(self.cu.data_path.data_alu_bus, -9223372036854775799 // 5)
        self.assertEqual(self.cu.data_path.get_positive_flag(), False)
        self.assertEqual(self.cu.data_path.get_zero_flag(), False)
        self.assertEqual(self.cu.data_path.reg_file.registers[Register.R3], -9223372036854775799 // 5)

    def test_reg_file(self):
        """Тестирование работы регистрового файла"""
        code: list[Operation] = read_code("resources/examples_correct/reg_file.json")

        self.set_program_and_run(code)

        self.assertEqual(self.cu.data_path.reg_file.registers[Register.R1], 42354326544525)
        self.assertEqual(self.cu.data_path.reg_file.registers[Register.R3], 42354326544525)
        self.assertEqual(self.cu.data_path.reg_file.registers[Register.R4], 42354326544525)

    def test_load(self):
        """Тестирование работы чтения из памяти"""
        code: list[Operation] = read_code("resources/examples_correct/load.json")

        self.set_program_and_run(code)

        self.assertEqual(self.cu.data_path.reg_file.registers[Register.R1], -2)
        self.assertEqual(self.cu.data_path.reg_file.registers[Register.R2], 24)

    def test_stoan(self):
        """Проверка работы записи в память"""
        code: list[Operation] = read_code("resources/examples_correct/stoan.json")

        self.set_program_and_run(code)

        prog_addr = self.get_start_addr()

        self.assertEqual(self.cu.data_path.memory[prog_addr].args[0].data, 2)

        self.assertEqual(self.cu.data_path.memory[prog_addr + 1].args[0].data, -9223372036854775808)

        self.assertEqual(self.cu.data_path.output_buffer[0], -9223372036854775808)

    def test_zero_flag(self):
        """Check zero flag"""
        code: list[Operation] = read_code("resources/examples_correct/zero_result.json")
        self.set_program_and_run(code)
        self.assertEqual(self.cu.data_path.get_zero_flag(), True)

    def test_out_of_memory_error(self):
        """Check out of memory error"""
        code: list[Operation] = read_code("resources/examples_correct/out_of_memory.json")
        with self.assertRaises(AssertionError):
            self.set_program_and_run(code)

    def test_incorrect_registers(self):
        """Check incorrect registers"""
        with self.assertRaises(ValueError) as cm:
            code: list[Operation] = read_code("resources/examples_correct/incorrect_regs.json")
            self.set_program_and_run(code)

        self.assertEqual(str(cm.exception), "'r9' is not a valid Register")

    def test_memory_allocation(self):
        """Сheck the correctness of memory allocation"""
        code: list[Operation] = read_code("resources/examples_correct/stoan.json")
        self.cu.set_program(code, [])
        self.assertEqual(len(self.cu.data_path.memory), AMOUNT_OF_MEMORY)

    def test_sub(self):
        """Check 'sub' operation"""
        code: list[Operation] = read_code("resources/examples_correct/sub_test.json")
        self.set_program_and_run(code)
        self.assertEqual(self.cu.data_path.reg_file.registers[Register.R1], 6)

    def test_large_amount_of_commands(self):
        """Check large amount of commands"""
        code: list[Operation] = read_code("resources/examples_correct/large_commands_amount.json")
        with self.assertRaises(AssertionError):
            self.set_program_and_run(code)

    def test_incorrect_latch_registers(self):
        """Check incorrect registers"""
        with self.assertRaises(ValueError):
            code: list[Operation] = read_code("resources/examples_correct/incorrect_latch_register.json")
            self.set_program_and_run(code)

    def test_dec(self):
        """Check 'dec' operation"""
        code: list[Operation] = read_code("resources/examples_correct/dec_test.json")
        self.set_program_and_run(code)
        self.assertEqual(self.cu.data_path.reg_file.registers[Register.R1], 19)

    def test_mul(self):
        """Check 'sub' operation"""
        code: list[Operation] = read_code("resources/examples_correct/mul_test.json")
        self.set_program_and_run(code)
        self.assertEqual(self.cu.data_path.reg_file.registers[Register.R1], 120)

    def test_div(self):
        """Check 'div' operation"""
        code: list[Operation] = read_code("resources/examples_correct/div_test.json")
        self.set_program_and_run(code)
        self.assertEqual(self.cu.data_path.reg_file.registers[Register.R1], -4611686018427387904)

    def test_too_large_argument(self):
        """Check large amount of commands"""
        code: list[Operation] = read_code("resources/examples_correct/too_large_argument.json")
        with self.assertRaises(AssertionError):
            self.set_program_and_run(code)

    def test_too_small_argument(self):
        """Check large amount of commands"""
        code: list[Operation] = read_code("resources/examples_correct/too_small_argument.json")
        with self.assertRaises(AssertionError):
            self.set_program_and_run(code)
