"""
Unit test for PyQt module
"""
import unittest

import src.machine.machine as machine
import src.qt.machine_run_file as machine_run


class QtTest(unittest.TestCase):
    """
    Unit-test for QT
    """

    def test_success_run(self):
        """Тест success run"""
        output, control_unit, log_stream = machine_run.run_machine(machine, "resources/correct/cat.json",
                                                                   "resources/input.txt")
        self.assertEqual(output,
                         '68(D) 111(o) 33(!) 59(;) 44(,) 10(\n) 64(@) 49(1) ')

    def test_not_correct_file(self):
        output, control_unit, log_stream = machine_run.run_machine(machine, "resources/correct/cat.json",
                                                                   "resourcesdsa/input.txt")

        self.assertEqual(output, None)
        self.assertEqual(control_unit, None)
        self.assertEqual(log_stream, None)

    def test_not_correct_code(self):
        output, control_unit, log_stream = machine_run.run_machine(machine, "resources/incorrect/operand_char.json",
                                                                   "resource/input.txt")

        self.assertEqual(output, None)
        self.assertEqual(control_unit, None)
        self.assertEqual(log_stream, None)
