"""
Unit-тесты для транслятора
"""

import unittest
import sys
from src.translation import isa, translator, preprocessor
from unittest.mock import patch,MagicMock

class TranslatorTest(unittest.TestCase):
    """Unit-тесты для транслятора"""
    args_ex = ['resources/source/hello.asm', 'resources/result/hello.json']
    source_ex = args_ex[0]

    def simple_test(self, input_file, output, correct):
        #Тест траслятора
        translator.main([input_file, output])

        result_code = isa.read_code(output)
        correct_code = isa.read_code(correct)

        self.assertEqual(len(result_code), len(correct_code))

        for idx, operation in enumerate(result_code):
            self.assertEqual(operation.opcode, correct_code[idx].opcode)
            self.assertEqual(operation.position, correct_code[idx].position)
            for arg_idx, arg in enumerate(operation.args):
                self.assertEqual(arg.mode, correct_code[idx].args[arg_idx].mode)
                self.assertEqual(arg.data, correct_code[idx].args[arg_idx].data)

    def test_cat(self):
        #Тест cat
        self.simple_test("resources/source/cat.asm", "resources/result/cat.json",
                         "resources/correct/cat.json")

    def test_prob5(self):
        #Тест prob5
        self.simple_test("resources/source/prob5.asm", "resources/result/prob5.json",
                         "resources/correct/prob5.json")

    def test_hello_world(self):
        #Тест hello
        self.simple_test("resources/source/hello.asm", "resources/result/hello.json",
                         "resources/correct/hello.json")

    # def test_translate(self):
    #
    #     with open(self.source_ex, "rt", encoding = "utf-8") as file:
    #         source_data = file.read()
    #
    #     code_ex = preprocessor.preprocessing(source_data)
    #     text_ex = code_ex.find('section .text')
    #
    #     data_ex = code_ex.find('section .data')
    #
    #     self.assertNotEqual(text_ex, -1) #check out: should raise value error
    #
    #     self.assertEqual(data_ex, -1)
    #     self.assertLess(data_ex, text_ex)




    #@patch('isa.is')
"""    def test_decode_register(self):
        args = ['add', '%r1', '%r2', '%r3']
        operation_ex = isa.Operation('add', 3)
        operation_type = isa.OperationType('register')

        self.assertIsNotNone(isa.Register(args.lower()))
        self.assertEqual(translator.decode_register(operation_ex, args), operation_ex)
        #self.assertEqual(operation_ex.is_corr_to_type((operation_type.REGISTER)), operation_type in op)

        #self.assertIsNotNone(translator.decode_register(isa.Operation, sys.argv))

        #self.assertEqual(translator.decode_register(Operation, sys.argv), operation)

    def test_check_operation_type(self):
        operationEx = isa.Operation('add', 1)
        operationTypeEx = isa.OperationType('mem')"""


"""
SURVIVED
--------
- src\ translation\ translator.py: (l: 94, c: 15) - mutation from <class 'ast.Eq'> to <class 'ast.LtE'>
- src\ translation\ translator.py: (l: 117, c: 4) - mutation from If_Statement to If_False
- src\ translation\ translator.py: (l: 193, c: 12) - mutation from AugAssign_Add to AugAssign_Div                         - src\translation\translator.py: (l: 325, c: 11) - mutation from <class 'ast.Lt'> to <class 'ast.NotEq'> 
"""



