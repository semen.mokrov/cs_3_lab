from PyQt5.QtWidgets import QApplication, QMainWindow, QMessageBox, QFileDialog, QTableWidgetItem

import sys
import src.qt.resources.design as design
import src.translation.translator as translator
import src.translation.isa as isa
import src.machine.machine as machine
import src.qt.machine_run_file as machine_runner


class MainWindow(QMainWindow, design.Ui_MainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.setupUi(self)

        self.setFixedSize(776, 620)
        self.result_name_translating = None

        self.pushButton.clicked.connect(self.translate_clk)
        self.plainTextEdit.textChanged.connect(self.validate_code)
        self.pushButton_3.clicked.connect(self.choose_file_clk)
        self.pushButton_4.clicked.connect(self.run_clk)

    def validate_code(self):
        if len(self.plainTextEdit.toPlainText()) > 0:
            self.pushButton.setEnabled(True)
        else:
            self.pushButton.setEnabled(False)

    def translate_clk(self):
        try:
            program_text = self.plainTextEdit.toPlainText()
            translated_text = translator.translate(program_text)

            filename, file_resolution = QFileDialog.getOpenFileName(self, 'Choose result file',
                                                                    r'C:\Users\Семен\PycharmProjects\software_testing\cs_3_lab',
                                                                    "JSON File (*.json)")
            isa.write_code(filename, translated_text)
            self.result_name_translating = filename

            self.before_run_setup()
        except Exception:
            msg = QMessageBox()
            msg.setWindowTitle('Translation error')
            msg.setText('Your code is not valid')
            msg.setIcon(QMessageBox.Icon.Critical)

            msg.exec_()

    def choose_file_clk(self):
        self.result_name_translating = self.choose_source_file("Choose machine code")
        self.before_run_setup()

    def run_clk(self):
        if self.result_name_translating is not None:
            output, control_unit, log_stream = self.run(self.result_name_translating)

            if output is not None and control_unit is not None and log_stream is not None:
                self.set_up_output(output)
                self.set_debug_logs(log_stream.getvalue())
                self.set_register_value(control_unit.data_path.reg_file.registers, control_unit.data_path.pc_counter)
                self.set_buses(control_unit.data_path.addr_alu_bus, control_unit.data_path.addr_bus,
                               control_unit.data_path.mem_bus, control_unit.data_path.data_alu_bus)
                self.set_flags(control_unit.data_path.get_zero_flag(), control_unit.data_path.get_positive_flag())
                self.after_run_setup()

    def choose_source_file(self, input_title):
        code_file_input, file_resolution_input = QFileDialog.getOpenFileName(self, input_title,
                                                                             r"C:\Users\Семен\PycharmProjects\software_testing\cs_3_lab",
                                                                             "JSON File (*.json)")
        return code_file_input

    def run(self, code_file_input):
        code_file_output, file_resolution_output = QFileDialog.getOpenFileName(self, "Choose input file",
                                                                               r"C:\Users\Семен\PycharmProjects\software_testing\cs_3_lab",
                                                                               "Text File (*.txt)")
        return machine_runner.run_machine(machine, code_file_input, code_file_output)

    def before_run_setup(self):
        self.pushButton.setEnabled(False)
        self.pushButton_3.setEnabled(False)
        self.pushButton_4.setEnabled(True)

        self.plainTextEdit.setEnabled(False)

    def after_run_setup(self):
        self.pushButton.setEnabled(True)
        self.pushButton_3.setEnabled(True)
        self.pushButton_4.setEnabled(False)

        self.plainTextEdit.setEnabled(True)
        self.plainTextEdit.setPlainText("")

    def set_up_output(self, output):
        self.plainTextEdit_3.setPlainText(output)

    def set_debug_logs(self, logs):
        self.plainTextEdit_2.setPlainText(logs)

    def set_register_value(self, registers, pc_counter):
        self.tableWidget.setItem(0, 0, QTableWidgetItem(str(registers[isa.Register.R1])))
        self.tableWidget.setItem(1, 0, QTableWidgetItem(str(registers[isa.Register.R2])))
        self.tableWidget.setItem(2, 0, QTableWidgetItem(str(registers[isa.Register.R3])))
        self.tableWidget.setItem(3, 0, QTableWidgetItem(str(registers[isa.Register.R4])))
        self.tableWidget.setItem(4, 0, QTableWidgetItem(str(registers[isa.Register.R5])))
        self.tableWidget.setItem(5, 0, QTableWidgetItem(str(pc_counter)))

    def set_buses(self, addr_alu_bus, addr_bus, mem_bus, data_alu_bus):
        self.tableWidget_4.setItem(0, 0, QTableWidgetItem(str(addr_alu_bus)))
        self.tableWidget_4.setItem(1, 0, QTableWidgetItem(str(addr_bus)))
        self.tableWidget_4.setItem(2, 0, QTableWidgetItem(str(mem_bus)))
        self.tableWidget_4.setItem(3, 0, QTableWidgetItem(str(data_alu_bus)))

    def set_flags(self, zero, positive):
        self.tableWidget_2.setItem(0, 0, QTableWidgetItem(str(zero)))
        self.tableWidget_2.setItem(1, 0, QTableWidgetItem(str(positive)))


def application():
    app = QApplication(sys.argv)
    window = MainWindow()

    window.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    application()
